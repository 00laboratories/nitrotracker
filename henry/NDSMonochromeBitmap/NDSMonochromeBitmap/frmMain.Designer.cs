﻿namespace NDSMonochromeBitmap
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonOpenRAW = new System.Windows.Forms.Button();
            this.PictureBoxViewport = new System.Windows.Forms.PictureBox();
            this.ButtonSaveRAW = new System.Windows.Forms.Button();
            this.ButtonOpenBMP = new System.Windows.Forms.Button();
            this.ButtonSaveBMP = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxSize = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxViewport)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonOpenRAW
            // 
            this.ButtonOpenRAW.Location = new System.Drawing.Point(12, 12);
            this.ButtonOpenRAW.Name = "ButtonOpenRAW";
            this.ButtonOpenRAW.Size = new System.Drawing.Size(80, 28);
            this.ButtonOpenRAW.TabIndex = 0;
            this.ButtonOpenRAW.Text = "Open RAW";
            this.ButtonOpenRAW.UseVisualStyleBackColor = true;
            this.ButtonOpenRAW.Click += new System.EventHandler(this.ButtonOpenRAW_Click);
            // 
            // PictureBoxViewport
            // 
            this.PictureBoxViewport.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.PictureBoxViewport.Location = new System.Drawing.Point(98, 12);
            this.PictureBoxViewport.Name = "PictureBoxViewport";
            this.PictureBoxViewport.Size = new System.Drawing.Size(128, 128);
            this.PictureBoxViewport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureBoxViewport.TabIndex = 1;
            this.PictureBoxViewport.TabStop = false;
            // 
            // ButtonSaveRAW
            // 
            this.ButtonSaveRAW.Location = new System.Drawing.Point(12, 80);
            this.ButtonSaveRAW.Name = "ButtonSaveRAW";
            this.ButtonSaveRAW.Size = new System.Drawing.Size(80, 28);
            this.ButtonSaveRAW.TabIndex = 2;
            this.ButtonSaveRAW.Text = "Save RAW";
            this.ButtonSaveRAW.UseVisualStyleBackColor = true;
            this.ButtonSaveRAW.Click += new System.EventHandler(this.ButtonSaveRAW_Click);
            // 
            // ButtonOpenBMP
            // 
            this.ButtonOpenBMP.Location = new System.Drawing.Point(12, 46);
            this.ButtonOpenBMP.Name = "ButtonOpenBMP";
            this.ButtonOpenBMP.Size = new System.Drawing.Size(80, 28);
            this.ButtonOpenBMP.TabIndex = 3;
            this.ButtonOpenBMP.Text = "Open BMP";
            this.ButtonOpenBMP.UseVisualStyleBackColor = true;
            this.ButtonOpenBMP.Click += new System.EventHandler(this.ButtonOpenBMP_Click);
            // 
            // ButtonSaveBMP
            // 
            this.ButtonSaveBMP.Location = new System.Drawing.Point(12, 114);
            this.ButtonSaveBMP.Name = "ButtonSaveBMP";
            this.ButtonSaveBMP.Size = new System.Drawing.Size(80, 28);
            this.ButtonSaveBMP.TabIndex = 4;
            this.ButtonSaveBMP.Text = "Save BMP";
            this.ButtonSaveBMP.UseVisualStyleBackColor = true;
            this.ButtonSaveBMP.Click += new System.EventHandler(this.ButtonSaveBMP_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Size:";
            // 
            // TextBoxSize
            // 
            this.TextBoxSize.Location = new System.Drawing.Point(44, 148);
            this.TextBoxSize.Name = "TextBoxSize";
            this.TextBoxSize.Size = new System.Drawing.Size(47, 20);
            this.TextBoxSize.TabIndex = 6;
            this.TextBoxSize.Text = "16";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(241, 181);
            this.Controls.Add(this.TextBoxSize);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonSaveBMP);
            this.Controls.Add(this.ButtonOpenBMP);
            this.Controls.Add(this.ButtonSaveRAW);
            this.Controls.Add(this.PictureBoxViewport);
            this.Controls.Add(this.ButtonOpenRAW);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.Text = "NDS Monochrome Bitmap";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxViewport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonOpenRAW;
        private System.Windows.Forms.PictureBox PictureBoxViewport;
        private System.Windows.Forms.Button ButtonSaveRAW;
        private System.Windows.Forms.Button ButtonOpenBMP;
        private System.Windows.Forms.Button ButtonSaveBMP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxSize;
    }
}

