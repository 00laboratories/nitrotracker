﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace NDSMonochromeBitmap
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
        }

        private void ButtonOpenRAW_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Nintendo DS RAW Files(*.RAW)|*.RAW";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                int size = Convert.ToInt32(TextBoxSize.Text);
                Bitmap b = openMonochromeBitmap(ofd.FileName, size, size);
                PictureBoxViewport.Image = b;
            }
        }

        private void ButtonOpenBMP_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Image Files(*.BMP)|*.BMP";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                PictureBoxViewport.Image = new Bitmap(ofd.FileName);
            }
        }

        private void ButtonSaveRAW_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Nintendo DS RAW Files(*.RAW)|*.RAW";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                saveMonochromeBitmap(sfd.FileName, new Bitmap(PictureBoxViewport.Image), PictureBoxViewport.Image.Width, PictureBoxViewport.Image.Height);
            }
        }

        private void ButtonSaveBMP_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Image Files(*.BMP)|*.BMP";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                PictureBoxViewport.Image.Save(sfd.FileName);
            }
        }

        private Bitmap openMonochromeBitmap(string path, int width, int height)
        {
            // read bytes from file.
            byte[] data = File.ReadAllBytes(path);
            string bits = "";

            // create string of the bits.
            for (int i = 0; i < data.Length; i++)
            {
                bits += ReverseString(Convert.ToString(data[i], 2).PadLeft(8, '0'));
            }

            Bitmap bitmap = new Bitmap(width, height);

            // iterate through each bit.
            int x = 0;
            int y = 0;
            for (int i = 0; i < data.Length * 8; i++)
            {
                if (bits[i] == '1')
                    bitmap.SetPixel(x, y, Color.Black);

                x++;
                if (x >= width)
                {
                    x = 0;
                    y++;
                }
            }

            return bitmap;
        }

        private void saveMonochromeBitmap(string path, Bitmap bitmap, int width, int height)
        {
            // create memory buffer.
            byte[] data = new byte[width * height];
            string bits = "";

            // create string of bits based on the bitmap image.
            int i = 0;
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color c = bitmap.GetPixel(x, y);
                    if (c.A == 255 && c.R == 0)
                        bits += "1";
                    else
                        bits += "0";
                    i++;
                    if (i >= 8)
                    {
                        // reverse the last 8 bits.
                        string lastpart = bits.Substring(bits.Length - 8);
                        lastpart = ReverseString(lastpart);
                        bits = bits.Substring(0, bits.Length - 8) + lastpart;
                        i = 0;
                    }
                }
            }

            // write to file
            File.WriteAllBytes(path, GetBytes(bits));
        }

        static class BinaryTable
        {
            public const char b00000000 = (char)0;
            public const char b00000001 = (char)1;
            public const char b00000010 = (char)2;
            public const char b00000011 = (char)3;
            public const char b00000100 = (char)4;
            public const char b00000101 = (char)5;
            public const char b00000110 = (char)6;
            public const char b00000111 = (char)7;
            public const char b00001000 = (char)8;
            public const char b00001001 = (char)9;
            public const char b00001010 = (char)10;
            public const char b00001011 = (char)11;
            public const char b00001100 = (char)12;
            public const char b00001101 = (char)13;
            public const char b00001110 = (char)14;
            public const char b00001111 = (char)15;
            public const char b00010000 = (char)16;
            public const char b00010001 = (char)17;
            public const char b00010010 = (char)18;
            public const char b00010011 = (char)19;
            public const char b00010100 = (char)20;
            public const char b00010101 = (char)21;
            public const char b00010110 = (char)22;
            public const char b00010111 = (char)23;
            public const char b00011000 = (char)24;
            public const char b00011001 = (char)25;
            public const char b00011010 = (char)26;
            public const char b00011011 = (char)27;
            public const char b00011100 = (char)28;
            public const char b00011101 = (char)29;
            public const char b00011110 = (char)30;
            public const char b00011111 = (char)31;
            public const char b00100000 = (char)32;
            public const char b00100001 = (char)33;
            public const char b00100010 = (char)34;
            public const char b00100011 = (char)35;
            public const char b00100100 = (char)36;
            public const char b00100101 = (char)37;
            public const char b00100110 = (char)38;
            public const char b00100111 = (char)39;
            public const char b00101000 = (char)40;
            public const char b00101001 = (char)41;
            public const char b00101010 = (char)42;
            public const char b00101011 = (char)43;
            public const char b00101100 = (char)44;
            public const char b00101101 = (char)45;
            public const char b00101110 = (char)46;
            public const char b00101111 = (char)47;
            public const char b00110000 = (char)48;
            public const char b00110001 = (char)49;
            public const char b00110010 = (char)50;
            public const char b00110011 = (char)51;
            public const char b00110100 = (char)52;
            public const char b00110101 = (char)53;
            public const char b00110110 = (char)54;
            public const char b00110111 = (char)55;
            public const char b00111000 = (char)56;
            public const char b00111001 = (char)57;
            public const char b00111010 = (char)58;
            public const char b00111011 = (char)59;
            public const char b00111100 = (char)60;
            public const char b00111101 = (char)61;
            public const char b00111110 = (char)62;
            public const char b00111111 = (char)63;
            public const char b01000000 = (char)64;
            public const char b01000001 = (char)65;
            public const char b01000010 = (char)66;
            public const char b01000011 = (char)67;
            public const char b01000100 = (char)68;
            public const char b01000101 = (char)69;
            public const char b01000110 = (char)70;
            public const char b01000111 = (char)71;
            public const char b01001000 = (char)72;
            public const char b01001001 = (char)73;
            public const char b01001010 = (char)74;
            public const char b01001011 = (char)75;
            public const char b01001100 = (char)76;
            public const char b01001101 = (char)77;
            public const char b01001110 = (char)78;
            public const char b01001111 = (char)79;
            public const char b01010000 = (char)80;
            public const char b01010001 = (char)81;
            public const char b01010010 = (char)82;
            public const char b01010011 = (char)83;
            public const char b01010100 = (char)84;
            public const char b01010101 = (char)85;
            public const char b01010110 = (char)86;
            public const char b01010111 = (char)87;
            public const char b01011000 = (char)88;
            public const char b01011001 = (char)89;
            public const char b01011010 = (char)90;
            public const char b01011011 = (char)91;
            public const char b01011100 = (char)92;
            public const char b01011101 = (char)93;
            public const char b01011110 = (char)94;
            public const char b01011111 = (char)95;
            public const char b01100000 = (char)96;
            public const char b01100001 = (char)97;
            public const char b01100010 = (char)98;
            public const char b01100011 = (char)99;
            public const char b01100100 = (char)100;
            public const char b01100101 = (char)101;
            public const char b01100110 = (char)102;
            public const char b01100111 = (char)103;
            public const char b01101000 = (char)104;
            public const char b01101001 = (char)105;
            public const char b01101010 = (char)106;
            public const char b01101011 = (char)107;
            public const char b01101100 = (char)108;
            public const char b01101101 = (char)109;
            public const char b01101110 = (char)110;
            public const char b01101111 = (char)111;
            public const char b01110000 = (char)112;
            public const char b01110001 = (char)113;
            public const char b01110010 = (char)114;
            public const char b01110011 = (char)115;
            public const char b01110100 = (char)116;
            public const char b01110101 = (char)117;
            public const char b01110110 = (char)118;
            public const char b01110111 = (char)119;
            public const char b01111000 = (char)120;
            public const char b01111001 = (char)121;
            public const char b01111010 = (char)122;
            public const char b01111011 = (char)123;
            public const char b01111100 = (char)124;
            public const char b01111101 = (char)125;
            public const char b01111110 = (char)126;
            public const char b01111111 = (char)127;
            public const char b10000000 = (char)128;
            public const char b10000001 = (char)129;
            public const char b10000010 = (char)130;
            public const char b10000011 = (char)131;
            public const char b10000100 = (char)132;
            public const char b10000101 = (char)133;
            public const char b10000110 = (char)134;
            public const char b10000111 = (char)135;
            public const char b10001000 = (char)136;
            public const char b10001001 = (char)137;
            public const char b10001010 = (char)138;
            public const char b10001011 = (char)139;
            public const char b10001100 = (char)140;
            public const char b10001101 = (char)141;
            public const char b10001110 = (char)142;
            public const char b10001111 = (char)143;
            public const char b10010000 = (char)144;
            public const char b10010001 = (char)145;
            public const char b10010010 = (char)146;
            public const char b10010011 = (char)147;
            public const char b10010100 = (char)148;
            public const char b10010101 = (char)149;
            public const char b10010110 = (char)150;
            public const char b10010111 = (char)151;
            public const char b10011000 = (char)152;
            public const char b10011001 = (char)153;
            public const char b10011010 = (char)154;
            public const char b10011011 = (char)155;
            public const char b10011100 = (char)156;
            public const char b10011101 = (char)157;
            public const char b10011110 = (char)158;
            public const char b10011111 = (char)159;
            public const char b10100000 = (char)160;
            public const char b10100001 = (char)161;
            public const char b10100010 = (char)162;
            public const char b10100011 = (char)163;
            public const char b10100100 = (char)164;
            public const char b10100101 = (char)165;
            public const char b10100110 = (char)166;
            public const char b10100111 = (char)167;
            public const char b10101000 = (char)168;
            public const char b10101001 = (char)169;
            public const char b10101010 = (char)170;
            public const char b10101011 = (char)171;
            public const char b10101100 = (char)172;
            public const char b10101101 = (char)173;
            public const char b10101110 = (char)174;
            public const char b10101111 = (char)175;
            public const char b10110000 = (char)176;
            public const char b10110001 = (char)177;
            public const char b10110010 = (char)178;
            public const char b10110011 = (char)179;
            public const char b10110100 = (char)180;
            public const char b10110101 = (char)181;
            public const char b10110110 = (char)182;
            public const char b10110111 = (char)183;
            public const char b10111000 = (char)184;
            public const char b10111001 = (char)185;
            public const char b10111010 = (char)186;
            public const char b10111011 = (char)187;
            public const char b10111100 = (char)188;
            public const char b10111101 = (char)189;
            public const char b10111110 = (char)190;
            public const char b10111111 = (char)191;
            public const char b11000000 = (char)192;
            public const char b11000001 = (char)193;
            public const char b11000010 = (char)194;
            public const char b11000011 = (char)195;
            public const char b11000100 = (char)196;
            public const char b11000101 = (char)197;
            public const char b11000110 = (char)198;
            public const char b11000111 = (char)199;
            public const char b11001000 = (char)200;
            public const char b11001001 = (char)201;
            public const char b11001010 = (char)202;
            public const char b11001011 = (char)203;
            public const char b11001100 = (char)204;
            public const char b11001101 = (char)205;
            public const char b11001110 = (char)206;
            public const char b11001111 = (char)207;
            public const char b11010000 = (char)208;
            public const char b11010001 = (char)209;
            public const char b11010010 = (char)210;
            public const char b11010011 = (char)211;
            public const char b11010100 = (char)212;
            public const char b11010101 = (char)213;
            public const char b11010110 = (char)214;
            public const char b11010111 = (char)215;
            public const char b11011000 = (char)216;
            public const char b11011001 = (char)217;
            public const char b11011010 = (char)218;
            public const char b11011011 = (char)219;
            public const char b11011100 = (char)220;
            public const char b11011101 = (char)221;
            public const char b11011110 = (char)222;
            public const char b11011111 = (char)223;
            public const char b11100000 = (char)224;
            public const char b11100001 = (char)225;
            public const char b11100010 = (char)226;
            public const char b11100011 = (char)227;
            public const char b11100100 = (char)228;
            public const char b11100101 = (char)229;
            public const char b11100110 = (char)230;
            public const char b11100111 = (char)231;
            public const char b11101000 = (char)232;
            public const char b11101001 = (char)233;
            public const char b11101010 = (char)234;
            public const char b11101011 = (char)235;
            public const char b11101100 = (char)236;
            public const char b11101101 = (char)237;
            public const char b11101110 = (char)238;
            public const char b11101111 = (char)239;
            public const char b11110000 = (char)240;
            public const char b11110001 = (char)241;
            public const char b11110010 = (char)242;
            public const char b11110011 = (char)243;
            public const char b11110100 = (char)244;
            public const char b11110101 = (char)245;
            public const char b11110110 = (char)246;
            public const char b11110111 = (char)247;
            public const char b11111000 = (char)248;
            public const char b11111001 = (char)249;
            public const char b11111010 = (char)250;
            public const char b11111011 = (char)251;
            public const char b11111100 = (char)252;
            public const char b11111101 = (char)253;
            public const char b11111110 = (char)254;
            public const char b11111111 = (char)255;
        }

        public static string ReverseString(string s)
        {
            char[] arr = s.ToCharArray();
            Array.Reverse(arr);
            return new string(arr);
        }

        public static byte[] GetBytes(string bitString)
        {
            return Enumerable.Range(0, bitString.Length / 8).
                Select(pos => Convert.ToByte(
                    bitString.Substring(pos * 8, 8),
                    2)
                ).ToArray();
        }
    }
}
